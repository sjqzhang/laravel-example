
## 系统依赖

```
os mac
php 8.3
node v16.20.2
```



### 增加依赖
```
        "infyomlabs/laravel-generator": "^6.0",
        "infyomlabs/adminlte-templates": "^6.0",
        "infyomlabs/laravel-ui-adminlte": "^5.2",
        "doctrine/dbal": "^3.6"
```


### 发布CURD基础资源


```
php artisan vendor:publish --provider="InfyOm\Generator\InfyOmGeneratorServiceProvider"
php artisan infyom:publish
```

### 生成授权UI

```
php artisan ui adminlte --auth
```


### 安装前端包及编译
```
npm install && npm run dev
```


### 配置数据库及生成表结构
```
php artisan migrate
```